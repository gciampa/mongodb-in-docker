// Connect to the MongoDB server running on the same machine.
conn = new Mongo();

// Use the test database.
db = conn.getDB("test");

// Read the password file.
var password = cat("/secrets/test-owner-password.txt");

// Strip leading and trailing whitespace.
password = password.replace(/(^\s+|\s+$)/g,'');

// Create test-owner in the test database, reading its password from
// /secrets/test-owner-password.txt . Grant this account read and write
// privileges over the test database, and also allow them to read from
// reporting (which is useful for statistics).
db.createUser(
  {
    user: "test-owner",
    pwd: password,
    roles: [
      { role: "readWrite", db: "test" },
      { role: "read", db: "reporting" }
    ]
  }
)
