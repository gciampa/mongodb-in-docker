# ex5-add-non-root

In our last example we created a root account. Once created, any unauthenticated
connection had limited access. The problem now is that the only alternative to
unauthenticated is authenticating as root and have complete authorization over
all databases in the server. This isn't much better than when we first set up
TLS.

In this example, we will create a non-root account, named `test-owner`,
with privileges to read and write the `test` database. Then, any client that
authenticates `test-owner` will have access to `test`, but nothing else. So if
there is ever a security breach related to `test-owner`, only data from the
`test` database could be leaked or damaged.

Let's get started.

```bash
cd ex5-add-non-root
```

Review `docker-compose.yml`. Notice that server has a new volume mounted --
`/docker-entrypoint-initdb.d`. When the server container is first created,
MongoDB runs scripts in this directory to initialize its databases. This gives
us a way to create our new `test-owner` account. We have created a local
directory by the same name, and have created a file named `account-test-owner.js`.
This local directory is mounted to `/docker-entrypoint-initdb.d` inside the
container by the `volumes:` entry for the server in docker-compose.yml.

Review `docker-entrypoint-initdb.d/account-test-owner.js` to get an idea of how
you can create a new MongoDB account using JavaScript. This script creates the
new user in the `test` database. Also note that `account-test-owner.js`
reads the test-owner's password from `/secrets/test-owner-password.txt`,
which you created in your local `secrets` directory, which is mounted to
`/secrets` inside the container (see `volumes:` for `server` in
`docker-compose.yml`). Again, this extra complication is because we want to
avoid committing sensitive information like passwords in our Git repository.

> :information_source: You may have noticed that there *are* passwords in this
README.md file. These are example passwords, and you are free to choose any
password you like. The real passwords only exist in the password files you
create and in the services as they run.

Return to `docker-compose.yml` and inspect the client definition. Note that
it will authenticate using the `test-owner` account in the `test` database.

Before we can run the examples, we need to create our password files and
generate our TLS files. Let's create our password files. Feel free to use any
passwords you like.

```bash
mkdir -p secrets
echo "some-password" > secrets/mongo-initdb-root-password.txt
echo "another-password" > secrets/test-owner-password.txt
```

Now let's generate the TLS files.

```bash
TLS_HOME="${PWD}/tls" docker-compose -f gen-tls.yml run --rm gen-tls client server
```

Now we can run our server.

```bash
docker-compose up --detach server
```

Take a look at the logs and notice the accounts being created.

```bash
docker-compose logs server
```

If the server wasn't ready when you ran the last command. Run it again until
it is and you see the messages indicating that the user accounts have been
created.

Last, run the client, type the correct password for `test-owner`,
and watch it succeed.

```bash
docker-compose run --rm client
```

Try running the client again and give a bad password (if you haven't already)
for `test-owner` and watch it fail. Then re-run the previous command and give it a good password and watch it succeed.

That's the end of this example. Let's cleanup.

```bash
docker-compose down
rm -r secrets tls
cd ..
```
