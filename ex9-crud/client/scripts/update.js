load('../lib/connect.js');
let db = connect();
db.cats.updateOne(
   // Find a cat whose name is Shadow
   { name: "Shadow" },
   {
      // Set its name to Pepper and add a NEW field status.
      $set: { 
         name: "Pepper",
         status: "hungry"
      }
   }
)
