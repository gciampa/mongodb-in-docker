conn = new Mongo("server:27017");
db = conn.getDB("test");

var password = cat("/secrets/test-owner-password.txt");

// strip leading and trailing whitespace
password = password.replace(/(^\s+|\s+$)/g,'');

db.auth("test-owner", password);

db.cats.insertOne(
  {
    color: "black",
    name: "shadow",
    age: 3
  }
);

db.cats.insertMany(
  [
    {
      color: "orange",
      name: "sherbert",
      age: 1
    },
    {
      color: "white",
      name: "casper",
      age: 8
    }
  ]
);
