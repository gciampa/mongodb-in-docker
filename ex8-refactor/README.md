# ex8-refactor

Before we add more functionality, we thought this would be a good time for a little spring cleaning. We have created two new subdirectories, client and server. We have moved files that belong to one or the other into its directory.

Client now has a subdirectory named scripts, and we've moved
create.js into it. We added this subdirectory because we are about to create a file for each CRUD operation, and thought this would be a good
way to keep them organized.

client.dockerfile has been renamed to Dockerfile and moved into client/. This file now also copies scripts/ into the root of the container (/scripts) and makes /scripts the working directory when the container runs.

The top-level docker-compose.yml file overrides the client's entrypoint.
Also, a new command is specified for the client. The purpose and functionality of these changes are described in those files.

If this refactoring was done correctly, all the old functionality should still be available. Let's test that by running it just like we did in our last example.

```bash
cd ex8-refactor
mkdir -p secrets
echo "some-password" > secrets/mongo-initdb-root-password.txt
echo "another-password" > secrets/test-owner-password.txt
TLS_HOME="${PWD}/tls" docker-compose -f gen-tls.yml run --rm gen-tls client server
docker-compose up --detach
```

After a few seconds, inspect the logs.

```bash
docker-compose logs
```

When you are satisfied, clean up.

```bash
docker-compose down
rm -r tls secrets
cd ..
```

With our project reorganized, we are ready to add more functionality.
