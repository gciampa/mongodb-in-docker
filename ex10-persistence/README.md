# ex10-persistence

In our last example, we saw that the data stored in the database is destroyed when the server container is destroyed. This behavior is desirable in a development environment as we need to run tests against the server in a known state. But this behavior is *not* desirable in a production environment. In production, we assume that the container will eventually crash, and we will need to destroy it and start another. But we do not want to lose all our data. So we need our data to outlive the container. Our goal in this example is to make our data persist across our server container instances.

To accomplish our goal, we're going to mount a local directory into the container into which MongoDB will read and write data files. The [documentation for the mongo docker image](https://hub.docker.com/_/mongo) tells us MongoDB will read and write data files to `/data/db`. We will mount a directory on our host system as `/data/db` in the container. Then, when MongoDB reads and writes to `/data/db` it will really be reading and writing to the directory on our host machine, thus preserving our data outside of the container.

Let's get started. First move into our example directory.

```bash
cd ex10-persistence
```

We need to create the local directory that will be mounted into the container. Let's call it `data`.

```bash
mkdir -p data
```

Now inspect `docker-compose.yml`. Notice the new volume entry for the `server` service. It mounts the `data` subdirectory (the one we just created) as `/data/db` inside the container.

Now the rest of the steps to setup our system are the same as the last example.

```bash
mkdir -p secrets
echo "some-password" > secrets/mongo-initdb-root-password.txt
echo "another-password" > secrets/test-owner-password.txt
TLS_HOME="${PWD}/tls" docker-compose -f gen-tls.yml run --rm gen-tls client server
docker-compose up --detach server
```

Now use the client to add some data to the database.

```bash
docker-compose run --rm client create.js
docker-compose run --rm client create.js
docker-compose run --rm client read.js
```

Now tear down the server, start a new one, and use the client to read its data.

```bash
docker-compose down
docker-compose up --detach server
docker-compose run --rm client read.js
```

(If the client times out, try running the last line again.)

If all went well, the new server container should have access to the same data that the previous server container did.

Inspect the contenst of the `data` directory.

```bash
ls data
```

This is where the MongoDB server is writing its data. If we want to reset the database *and* its data, we can tear down the container *and* delete the contents of this directory. Let's try it.

```bash
docker-compose down
rm -r ./data
```

Now, create a new data directory, start a new server container, and read its data.

```bash
mkdir -p data
docker-compose up --detach server
docker-compose run --rm client read.js
```

It should be empty.

That concludes this example. When you are satisfied, clean up.

```bash
docker-compose down
rm -r tls secrets data
cd ..
```
